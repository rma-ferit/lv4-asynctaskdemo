package hr.ferit.brunozoric.asynctaskdemo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val countries = arrayOf("Croatia", "Uganda", "Italy", "Utopia")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpUi()
    }

    private fun setUpUi() {
        getCountryInfoAction.setOnClickListener{
            CountriesTask(this).execute(*countries)
        }
    }

    fun displayProgressDialog(){
        progressDisplay.visibility = View.VISIBLE
    }

    fun hideDisplayProgressDialog(){
        progressDisplay.visibility = View.GONE
    }

    fun updateProgress(progress: Int){
        progressDisplay.setProgress(progress)
    }

    fun displayResults(results: List<String>) {
        countriesInfo.adapter = ArrayAdapter(
            this,
            android.R.layout.simple_list_item_1,
            results
            )
    }
}
