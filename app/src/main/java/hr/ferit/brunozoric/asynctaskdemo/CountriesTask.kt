package hr.ferit.brunozoric.asynctaskdemo

import android.os.AsyncTask
import java.lang.ref.WeakReference

class CountriesTask(mainActivity: MainActivity): AsyncTask<String, Int, List<String>>() {

    val activity: WeakReference<MainActivity> = WeakReference(mainActivity)

    override fun onPreExecute() {
        super.onPreExecute()
        (activity.get() as MainActivity).displayProgressDialog()
    }

    override fun onProgressUpdate(vararg values: Int?) {
        super.onProgressUpdate(*values)
        val progress = values.first() ?: 0
        (activity.get() as MainActivity)?.updateProgress(progress)
    }

    override fun doInBackground(vararg params: String?): List<String> {

        val countriesInfo = mutableListOf<String>()

        for(i in 0 until params.size){
            countriesInfo.add("i <3 ${params[i]}")
            val percent = i.toDouble() / params.size * 100
            publishProgress(percent.toInt())
            Thread.sleep(1000)
        }
        return countriesInfo
    }

    override fun onPostExecute(result: List<String>?) {
        super.onPostExecute(result)
        (activity.get() as MainActivity)?.let{
            it.hideDisplayProgressDialog()
            val results = result ?: listOf()
            it.displayResults(results)
        }
    }
}
